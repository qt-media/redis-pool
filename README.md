# Redis pool

OTP worker pool library for Redis (eredis by default, hierdis is supported).

Depends on eredis in rebar.config, so if you need hierdis, add it to your rebar.config (see example in this library's rebar.config).

NOTE: Redis pool _application_ doesn't depend directly on eredis or hierdis (see redis_pool.app.src), so you need to manually set dependence on eredis or hierdis in yours %appname%.app.src!

## Build

    $ make

## Usage

By default (for backward compatibility) the library doesn't start any pools on application startup. You should start all pools manually by hand from your %appname%:

 - redis_pool:start() — start all pools from config;
 - redis_pool:start([my_pool_1, my_pool_5]) - start custom pools from config.

Please, note, for testing purpose ```make run``` call redis_pool:start() automatically on node startup, but redis_pool:start/0 and redis_pool:start/1 is idempotent, so you can call them over and over again:

```
    $ make run
    %% Note, all pools already started, but you can run redis_pool:start() over and over
    1> redis_pool:start().
    ok
```

or

```
    $ make run
    %% Note, all pools already started, but you can run redis_pool:start([my_pool_1, my_pool_5]) over and over
    1> redis_pool:start([my_pool_1, my_pool_5]).
    ok
```

```
    redis_pool:with_redis(bar, fun(C) ->
        eredis:q(C, ["GET", foo])
    end)

    redis_pool:with_redis(baz, fun(C) ->
        eredis:q(C, ["GET", foo])
    end, 10000)
```

You can view live statistics about single pool or all of them:

```
    1> redis_pool:stats().
    [[{pool,redis_pool_1_redis},
      {supervisor,<0.66.0>},
      {options,[{workers,1},
                {worker,{redis_pool_worker,["127.0.0.1",6379,0]}},
                {overrun_warning,infinity},
                {overrun_handler,{error_logger,warning_report}},
                {workers,100},
                {worker,{wpool_worker,undefined}},
                {worker_opt,[]}]},
      {size,1},
      {next_worker,1},
      {total_message_queue_len,0},
      {workers,[{1,[{message_queue_len,0},{memory,5856}]}]}],
      ...
    2> redis_pool:stats(redis_pool_2).
    [[{pool,redis_pool_2_redis},
    ...
```

## Error Handling

`redis_pool:with_redis` could throw `no_workers` or exit with `timeout`.

- `no_workers` the pool is empty (all workers are dead, although that should not happen; **do not catch it**)
- `timeout` command timeouted (*default timeout is 5 seconds*).

Note: `timeout` **does not** include time for picking up a worker.

If you are using eredis, check that you handling eredis:q timeout error also.

```
    try
        redis_pool:with_redis(bar, fun(C) ->
            try
                eredis:q(C, ["GET", foo])
            catch exit:{timeout, {gen_server, call, _}} -> {error, timeout}
            end
        end)
    catch exit:{timeout, {gen_server, call, _}} -> % retry now or later
    end
```

## Tests

    $ make test

## Config

```
    {redis_pool, [
        {redis_backend, eredis},
        {redises, [
            {abonent_storage0, {["127.0.0.1", 6379, 0], 100}},
            {abonent_storage1, {["127.0.0.1", 6379, 0], 100}},
        ]}
    ]}.
```

## Usage with redis sentinel

Deps:

```
{deps, [
    {eredis_sentinel, "*", {git, "https://github.com/miros/eredis_sentinel.git"}}
]}.
```

Config:

```
    {redis_pool, [
        {redis_backend, eredis},
        {redises, [
            {abonent_storage0, {["master-name-1", 0], 100}},
            {abonent_storage1, {["master-name-2", 0], 100}},
        ]},
        {redis_factory, {eredis_sentinel, connect_eredis, []}}
    ]},

    {eredis_sentinel, [
      {sentinel_addrs, [["some-host", 26379], ["some-other-host", 26379]]}
    ]}.
```

### Available backends

* eredis (**default**)
* hierdis

See `rebar.config` for the current versions.

## Changelog

### 2.0.0 / 12/12/2018

* Depend only on `eredis` by default. Removed `hierdis` from `deps` in `rebar.config` and `rebar.lock`.

### 1.2.0 / 31/05/2016

* hierdis 1.0.0

### 1.1.0 / 13/05/2016

* random_worker strategy

### 1.0.0 / 17/03/2016

* switched to inaka/worker_pool
* redis_pool:with_redis/2 now has 5 seconds timeout
* improved readme
* redis_pool:with_redis/2 now throws no_workers or timeout errors
* added redis_pool:stats/0 and redis_pool:stats/1
