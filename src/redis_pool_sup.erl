-module(redis_pool_sup).

-behaviour(supervisor).

%% API
-export([start_link/0,
         start_children/1]).

%% Supervisor callbacks
-export([init/1]).

-define(CHILD_WITH_ARGS(I, Type, Args), {I, {I, start_link, Args}, permanent, 5000, Type, [I]}).

-include("redis_pool.hrl").

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

-spec start_children([name()]) -> [{'ok','undefined' | pid()}].
start_children(Names) ->
    PoolSpecsRedises = redises_pool_specs(Names, redis_pool:get_redises_config()),
    [start_child(Spec) || Spec <- PoolSpecsRedises].
    
%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    {ok, {{one_for_one, 10000, 1}, []}}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

start_child(Spec) ->
  case supervisor:start_child(?MODULE, Spec) of
    {ok, _} ->
      nop;
    {error, already_present} ->
      nop;
    {error, {already_started, _}} ->
      nop;
    {error, Error} ->
      error({redis_pool_start_child_error, Error})
  end.

-spec redises_pool_specs([name()], list()) -> [supervisor:child_spec()].
redises_pool_specs(Names, Config) when Names == [] orelse Config == [] ->
    [];

redises_pool_specs(Names, Config) ->
    specs(Names, Config, redis_pool_worker, []).

-spec specs([name()], list(), atom(), list()) -> [supervisor:child_spec()].
specs(Names, Config, WorkerType, Acc) ->
    Redises = lists:filter(fun({Name, _}) -> lists:member(Name, Names) end, Config),
    case Redises of
        [] ->
            [];
        Redises ->
            lists:reverse(lists:foldl(fun(X, Acc2) -> [child_spec(X, WorkerType) | Acc2] end, Acc, Redises))
    end.

-spec child_spec({name(), {list(), non_neg_integer()}}, atom()) -> supervisor:child_spec().
child_spec({Name, {Args, PoolSize}}, WorkerType) ->
  PoolName = redis_pool_utils:name_for(Name),

  RedisFactory = application:get_env(redis_pool, redis_factory, {redis_pool_utils, connect_redis, []}),
  RedisFactoryCb = new_redis_factory_callback(Args, RedisFactory),

  PoolArgs = [
      {workers, PoolSize},
      {worker, {WorkerType, [RedisFactoryCb]}}
  ],
  {PoolName, {wpool, start_pool, [PoolName, PoolArgs]}, permanent, 5000, supervisor, [wpool]}.

new_redis_factory_callback(RedisArgs, {M, F, AdditionalArgs}) ->
  fun() -> apply(M, F, RedisArgs ++ AdditionalArgs) end.
