-module(redis_pool_utils).

%% API
-export([
  name_for/1,
  connect_redis/3
]).

-export([to_atom/1,
         to_list/1]).

-include("redis_pool.hrl").

-define(RECONNECT_SLEEP, 100). % ms. only for eredis
-define(CONNECT_TIMEOUT, 5000). % ms.

%% ===================================================================
%% API functions
%% ===================================================================

%% Возвращает имя пула для данного инстанса redis
-spec name_for(name()) -> atom().
name_for(Name) ->
  list_to_atom(to_list(Name) ++ "_redis").

-spec connect_redis(list(), non_neg_integer(), non_neg_integer()) -> {'ok', pid() | binary()}.
connect_redis(Host, Port, DB) ->
  case redis_pool:get_redis_backend() of
        eredis ->
            {ok, _Conn} = eredis:start_link(Host, Port, DB, "", ?RECONNECT_SLEEP, ?CONNECT_TIMEOUT);
        hierdis ->
            {ok, _Conn} = hierdis:connect(Host, Port, DB, ?CONNECT_TIMEOUT)
    end.

to_atom(Val) when is_atom(Val) -> Val;
to_atom(Val) when is_list(Val) -> list_to_atom(Val).

to_list(Val) when is_list(Val) -> Val;
to_list(Val) when is_atom(Val) -> atom_to_list(Val);
to_list(Val) when is_integer(Val) -> integer_to_list(Val).

%% ===================================================================
%% Internal functions
%% ===================================================================
