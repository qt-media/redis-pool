-module(redis_pool).

%% API
-export([start/0,
         start/1,
         stop/0]).

-export([with_redis/2,
         with_redis/3,
         stats/0,
         stats/1,
         get_redis_backend/0,
         get_redis_config/1,
         get_redises_config/0]).

-include("redis_pool.hrl").

%% ===================================================================
%% Application API
%% ===================================================================

%% NB: Please, don't fix/rewrite/optimize the start/0 and start/1. First of all,
%%     we need run application and load application env via application:ensure_all_started,
%%     so, we can't do something like start() -> start(all_pool_names()).
-spec start() -> 'ok'.
start() ->
    {ok, _} = application:ensure_all_started(redis_pool),
    redis_pool_sup:start_children(all_pool_names()),
    ok.

-spec start([name()]) -> 'ok'.
start(Names) ->
    {ok, _} = application:ensure_all_started(redis_pool),
    redis_pool_sup:start_children(Names),
    ok.

-spec stop() -> 'ok'.
stop() ->
    ok = application:stop(redis_pool).

%% @doc If the pool is empty, +no_workers+ will be thrown.
%% @throws no_workers
-spec with_redis(name(), [fun((...) -> any())]) -> any().
with_redis(Name, Fun) ->
    with_redis(Name, Fun, ?TIMEOUT).

%% @throws no_workers
-spec with_redis(name(), [fun((...) -> any())], timeout()) -> any().
with_redis(Name, Fun, Timeout) ->
    wpool:call(redis_pool_utils:name_for(Name), {command, Fun}, random_worker, Timeout).

-spec get_redis_backend() -> 'eredis' | 'hierdis'.
get_redis_backend() ->
    application:get_env(redis_pool, redis_backend, ?DEFAULT_REDIS_BACKEND).

-spec get_redises_config() -> list().
get_redises_config() ->
    application:get_env(redis_pool, redises, []).

get_redis_config(Name) ->
    PoolConfig = get_redises_config(),
    case proplists:get_value(Name, PoolConfig) of
        undefined ->
            error({unknown_redis, Name});
        RedisConfig ->
            RedisConfig
    end.

-spec stats() -> [stats()].
stats() ->
    lists:map(fun stats/1, all_pool_names()).

-spec stats(name()) -> stats().
stats(Name) ->
    wpool:stats(redis_pool_utils:name_for(Name)).

%% ===================================================================
%% Internal functions
%% ===================================================================

-spec all_pool_names() -> [name()].
all_pool_names() ->
    proplists:get_keys(get_redises_config()).
