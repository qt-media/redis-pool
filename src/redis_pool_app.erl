-module(redis_pool_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-include("redis_pool.hrl").

%% ===================================================================
%% Application callbacks
%% ===================================================================

-spec start(any(), any()) -> no_return().
start(_StartType, _StartArgs) ->
    ok = start_redis_backend(),
    redis_pool_sup:start_link().

-spec stop(any()) -> 'ok'.
stop(_State) ->
    ok.

%% ===================================================================
%% Internal functions
%% ===================================================================

-spec start_redis_backend() -> 'ok' | {'error', term()}.
start_redis_backend() ->
    case application:load(redis_pool) of
        ok ->
            ok = start_redis_backend_ll();
        {error, {already_loaded, redis_pool}} ->
            ok = start_redis_backend_ll();
        Error ->
            Error
    end.

-spec start_redis_backend_ll() -> 'ok'.
start_redis_backend_ll() ->
    RedisBackend = redis_pool:get_redis_backend(),
    {ok, _} = application:ensure_all_started(RedisBackend),
    ok.

