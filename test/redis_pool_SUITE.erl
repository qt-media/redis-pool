-module(redis_pool_SUITE).

-export([all/0,
         group/1,
         groups/0,

         init_per_group/2,
         end_per_group/2,

         init_per_testcase/2,
         end_per_testcase/2]).

-export([start_all_pools_eredis/1,
         start_custom_pools_eredis/1,
         start_with_no_pools_eredis/1
         ]).

-include_lib("eunit/include/eunit.hrl").
-include_lib("common_test/include/ct.hrl").


all() ->
    [{group, main}].

group(main) ->
    [{timetrap, {seconds, 60}}].

groups() ->
    [
     {main,
      [],
      [
        start_all_pools_eredis,
        start_custom_pools_eredis,
        start_with_no_pools_eredis
     ]
    }
   ].

init_per_group(_, Config) ->
    Config.

end_per_group(_, _Config) ->
    ok.

init_per_testcase(_, Config) ->
    Config.

end_per_testcase(_, _Config) ->
    ok.


%%%===================================================================
%%% Test cases
%%%===================================================================

start_all_pools_eredis(_Config) ->
    app_env(redis_pools(), eredis),

    meck:new(eredis),
    meck:expect(eredis, start_link, fun(_, _, _, _, _, _) -> {ok, list_to_pid("<0.999.0>")} end),

    redis_pool:start(),

    ?assertMatch([{redis_pool_5_redis, _, supervisor, [wpool]},
                  {redis_pool_4_redis, _, supervisor, [wpool]},
                  {redis_pool_3_redis, _, supervisor, [wpool]},
                  {redis_pool_2_redis, _, supervisor, [wpool]},
                  {redis_pool_1_redis, _, supervisor, [wpool]}], supervisor:which_children(redis_pool_sup)),

    ?assertExit(no_workers, redis_pool:with_redis(redis_pool_0, fun(X) -> X end)),
    ?assertEqual(list_to_pid("<0.999.0>"), redis_pool:with_redis(redis_pool_1, fun(X) -> X end)),

    redis_pool:stop(),
    meck:unload(eredis),
    ok.

start_custom_pools_eredis(_Config) ->
    app_env(redis_pools(), eredis),

    meck:new(eredis),
    meck:expect(eredis, start_link, fun(_, _, _, _, _, _) -> {ok, list_to_pid("<0.999.0>")} end),

    redis_pool:start([redis_pool_1, redis_pool_5]),

    ?assertMatch([{redis_pool_5_redis, _, supervisor, [wpool]},
                  {redis_pool_1_redis, _, supervisor, [wpool]}], supervisor:which_children(redis_pool_sup)),

    ?assertExit(no_workers, redis_pool:with_redis(redis_pool_0, fun(X) -> X end)),
    ?assertEqual(list_to_pid("<0.999.0>"), redis_pool:with_redis(redis_pool_1, fun(X) -> X end)),

    redis_pool:stop(),
    meck:unload(eredis),
    ok.

start_with_no_pools_eredis(_Config) ->
    app_env([], eredis),

    redis_pool:start(),

    ?assertMatch([], supervisor:which_children(redis_pool_sup)),

    ?assertExit(no_workers, redis_pool:with_redis(redis_pool_0, fun(X) -> X end)),
    ?assertExit(no_workers, redis_pool:with_redis(redis_pool_1, fun(X) -> X end)),

    redis_pool:stop(),
    ok.

redis_pools() ->
    [{redis_pool_1, {["127.0.0.1", 6379, 0], 1}},
     {redis_pool_2, {["127.0.0.1", 6379, 0], 2}},
     {redis_pool_3, {["127.0.0.1", 6379, 0], 3}},
     {redis_pool_4, {["127.0.0.1", 6379, 0], 4}},
     {redis_pool_5, {["127.0.0.1", 6379, 0], 5}}].


app_env(RedisPools, Backend) ->
    application:set_env(redis_pool, redises,            RedisPools),
    application:set_env(redis_pool, redis_backend,      Backend),
    ok.
