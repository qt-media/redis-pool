-module(redis_pool_worker).

-behavior(gen_server).

%% API
-export([start_link/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {connection :: pid() | binary()
}).

%%%===================================================================
%%% API functions
%%%===================================================================

start_link(RedisFactory) ->
    gen_server:start_link(?MODULE, [RedisFactory], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([RedisFactory]) ->
    {ok, Conn} = RedisFactory(),
    {ok, #state{connection = Conn}}.

handle_call({command, Fun}, _From, #state{connection = Conn} = State) ->
    {reply, Fun(Conn), State};

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
