-define(DEFAULT_REDIS_BACKEND, eredis).
-define(TIMEOUT, 5000). % ms.

-type name() :: atom() | list().
-type worker_stats() :: [ {messsage_queue_len, non_neg_integer()}
                        | {memory, pos_integer()}
                        ].
-type stats() :: [ {pool, atom()}
                 | {supervisor, pid()}
                 | {options, [tuple()]}
                 | {size, non_neg_integer()}
                 | {next_worker, pos_integer()}
                 | {total_message_queue_len, non_neg_integer()}
                 | {workers, [{pos_integer(), worker_stats()}]}
                 ].
-export_type([name/0, stats/0]).
